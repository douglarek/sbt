import akka.actor.{Actor, ActorSystem, Props}
import akka.testkit.CallingThreadDispatcher
import org.json4s.native.JsonMethods.parse

class Persion(val firstName: String, val lastName: String) {

  private var _age = 0
  def age = _age

  // method name is: age_=, this is a
  def age_=(newAge: Int) = _age = newAge

  def fullName() = firstName + " " + lastName

  override def toString() = fullName()
}

class Connection {
  def close() = println("close Connection")

}

trait ForEachAble[A] {
  def iterator: java.util.Iterator[A]

  def foreach(f: A => Unit) = {
    val iter = iterator
    while (iter.hasNext())
      f(iter.next)
  }
}

trait JsonAble {
  def toJson() = parse(this.toString())
}

object Hi {

  def withClose(closeAble: { def close(): Unit }, op: { def close(): Unit } => Unit) = {
    try {
      op(closeAble)
    } finally {
      closeAble.close()
    }
  }

  def withClose[A <: { def close(): Unit }, B](closeAble: A)(op: A => B): B =
    try {
      op(closeAble)
    } finally {
      closeAble.close()
    }

  def main(args: Array[String]) = {
    val obama = new Persion("Barack", "Obama")

    println(obama)

    println(obama.firstName)
    println(obama.lastName)
    obama.age = 51
    println(obama.age)

    val conn: Connection = new Connection()

    withClose(conn, _ => println("do something with Connection"))

    withClose(conn)(_ => println("do something with Connection"))

    val msg = withClose(conn) { conn =>
      {
        println("do something with Connection")
        123456
      }
    }
    println(msg.toString())

    val list = new java.util.ArrayList[Int]() with ForEachAble[Int] with JsonAble

    list.add(1)
    list.add(2)

    list.foreach { println(_) }

    println(list.toJson())

    class ScalaCurrentVersion(val url: String) {
      lazy val source = {
        println("fetching from url ...")
        scala.io.Source.fromURL(url).getLines().toList
      }
      lazy val majorVersion = source.find(_.contains("version.major"))
      lazy val minorVersion = source.find(_.contains("version.minor"))
    }

    val version = new ScalaCurrentVersion("https://raw.github.com/scala/scala/2.11.x/build.number")
    version.majorVersion.foreach(println(_))
    version.minorVersion.foreach(println(_))

    def getProperty(name: String): Option[String] = {
      val value = System.getProperty(name)
      if (value != null) Some(value) else None
    }
    val osName = getProperty("os.name")

    osName match {
      case Some(value) => println(value)
      case _ => println("non")
    }

    println(osName.getOrElse("none"))

    osName.foreach(println(_))

    val file = List("warn 2013 msg", "warn 2012 msg", "error 2013 msg", "warn 2013 msg")

    def wordcount(str: String) = str.split(" ").count("msg" == _)

    def foldLeft(list: List[Int])(init: Int)(f: (Int, Int) => Int): Int = {
      list match {
        case List() => init
        case head :: tail => foldLeft(tail)(f(init, head))(f)
      }
    }

    val num = foldLeft(file.map(wordcount))(0)(_ + _)

    implicit val system = ActorSystem()

    class EchoServer(name: String) extends Actor {
      def receive = {
        case msg => println("server" + name + " echo " + msg + " by" + Thread.currentThread().getName())
      }
    }

    val echoServers = (1 to 10).map(x => system.actorOf(Props(new EchoServer(x.toString)).withDispatcher(CallingThreadDispatcher.Id)))

    (1 to 10).foreach(msg => echoServers(scala.util.Random.nextInt(10)) ! msg.toString())

    system.shutdown()

  }

}
