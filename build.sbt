lazy val commonSettings = Seq(
  name := "sample",
  version := "1.0",
  scalaVersion := "2.11.7"
)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(
    resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % "2.4-SNAPSHOT",
      "com.typesafe.akka" %% "akka-testkit" % "2.4-SNAPSHOT",
      "org.json4s" %% "json4s-native" % "3.2.11"
    )
  )

scalacOptions ++= Seq("-language:reflectiveCalls")
